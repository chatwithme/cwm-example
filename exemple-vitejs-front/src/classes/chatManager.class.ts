import { CwmChat, CwmCredentialsHeader, CwmMessage } from "@chatwithme/client"
import { AxiosResponse } from "axios"
import LoginService from "../services/login.service"
import { TalkerButton } from "./talkerButton.class"

export class ChatManager {

    private currentTalker: TalkerButton | null = null
    private messageByTalker: { [talkerId: string]: CwmMessage[] } = { };

    private messageContainer: HTMLDivElement | null = null
    private messageInput: HTMLInputElement | null = null

    constructor() {
        this.messageContainer = document.querySelector<HTMLDivElement>("#message-container")
        this.messageInput = document.querySelector<HTMLInputElement>('#message-text')

        this.initMemberList()
        this.initCwm()
    }

    initCwm() {
        CwmChat
        .start(LoginService.API_LINK + '/chat', new CwmCredentialsHeader('APP_TOKEN', LoginService.instance.token))
        .then(() => {
            this.initCwmSocket()
            this.onSelectTalker(this.currentTalker!)
        })
        .catch(console.warn)
    }

    initCwmSocket() {
        CwmChat.subscribeMessage((message) => {
            console.log(message)
            let talkerId: string = message.isMe() ? message.getRecipient() : message.getSender()
            
            if (this.messageByTalker.hasOwnProperty(talkerId)) {
                this.messageByTalker[talkerId].push(message)
            } else {
                this.messageByTalker[talkerId] = [message]
            }

            if(this.currentTalker && this.currentTalker.user.id == talkerId) {
                this.displayOneMessage(message)
            }
        })
    }

    initMemberList() {
        if (!LoginService.instance.isLogin) return

        document.querySelector<HTMLButtonElement>("#send-button")!.onclick = () => { this.sendMessage() }

        LoginService
            .instance
            .getAllUser()
            .then((response: AxiosResponse) => {
                console.log(response.data)
                this.onUsersLoaded(response.data)
            })
    }

    onUsersLoaded(users: any) {
        const memberContainer: HTMLDivElement = document.querySelector<HTMLDivElement>("#member-list")!
        
        for (let u = 0; u < users.length; u ++) {
            const user = users[u]

            const button = new TalkerButton(user)
            button.buttonHtml.onclick = () => {
                this.onSelectTalker(button)
            }
            memberContainer.appendChild(button.buttonHtml)

            if ( u == 0) {
                this.currentTalker = button
            }
        }

    }

    onSelectTalker(button: TalkerButton) {
        const currentConversationTitle: HTMLHeadingElement = document.querySelector<HTMLDivElement>("#currentConversationTitle")!
        currentConversationTitle.innerText = `Your discussion with ${button.user.name}`;

        if (this.currentTalker != null) {
            this.currentTalker.unSelect()
        }

        this.currentTalker = button

        if (this.messageByTalker[button.user.id] == undefined || this.messageByTalker[button.user.id].length == 0) {
            CwmChat.getMessageHistoryWith({
                recipientId: button.user.id
            }).then((messages) => {
                this.messageByTalker[button.user.id] = messages;
                this.displayMessages()
            })
        } else {
            this.displayMessages()
        }

        button.select()
    }

    displayMessages() {
        if (this.currentTalker == null) return
        if (this.messageContainer == null)  return;
        if (Array.isArray(this.messageByTalker[this.currentTalker.user.id]) == false) return;;

        this.clearMessages()

        for(const msg of this.messageByTalker[this.currentTalker.user.id]) {
            this.displayOneMessage(msg)
        }
    }

    displayOneMessage(message: CwmMessage) {
        if (this.messageContainer == null)  return;

        // Create the row of the message
        const msgNode = document.createElement('div')
        msgNode.classList.add('d-flex')

        if(message.isMe()) {
            msgNode.classList.add('flex-row-reverse')
        }

        // Create span to contain the content of the message
        const msgSpan = document.createElement('span')
        msgSpan.innerText = message.getContent()
        msgSpan.classList.add('btn', message.isMe() ? 'btn-primary' : 'btn-info')
        msgNode.appendChild(msgSpan)

        this.messageContainer.appendChild(msgNode)
    }

    clearMessages() {
        if (this.messageContainer == null)  return;

        while(this.messageContainer.lastChild != null) {
            this.messageContainer.removeChild(this.messageContainer.lastChild)
        }
    }

    sendMessage() {
        if(this.currentTalker == undefined) return
        if(this.messageInput == undefined) return

        const message: string = this.messageInput.value
        CwmChat.sendMessage(new CwmMessage(message, this.currentTalker.user.id))
    }
}