import { UserDto } from "../interfaces/user.dto";

export class TalkerButton {

    private _user: UserDto;
    private _buttonHtml: HTMLButtonElement

    constructor(user: UserDto) {
        this._user = user;

        this._buttonHtml = document.createElement('button')
        this._buttonHtml.innerText = this._user.name
        this._buttonHtml.classList.add('btn', 'btn-secondary')
    }

    get buttonHtml(): HTMLButtonElement {
        return this._buttonHtml
    }

    get user(): UserDto {
        return this._user
    }

    select() {
        this._buttonHtml.classList.remove('btn-secondary')
        this._buttonHtml.classList.add('btn-warning')
    }

    unSelect() {
        this._buttonHtml.classList.remove('btn-warning')
        this._buttonHtml.classList.add('btn-secondary')
    }  
}