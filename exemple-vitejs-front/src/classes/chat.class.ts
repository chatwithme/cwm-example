import { ChatManager } from "./chatManager.class";
import LoginService from "../services/login.service";

export default class Chat {
    constructor() {
        LoginService.instance.onLogin(() => {
            new ChatManager()
        })
    }
}
