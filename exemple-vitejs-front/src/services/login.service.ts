import axios from "axios";
import { UserDto } from "../interfaces/user.dto";

export default class LoginService {

    public static readonly API_LINK = 'http://localhost:3000'
    static INSTANCE: LoginService

    private _loginButton: HTMLButtonElement
    // @ts-ignore
    private _token: string = ""
    private _isLogin: boolean
    public _onLogin: Function = () => {}

    protected constructor() {
        this._loginButton = document.querySelector<HTMLButtonElement>("#login")!
        this._isLogin = false
        this._loginButton.onclick = this.login
    }

    static get instance() {
        if(LoginService.INSTANCE == undefined) {
            LoginService.INSTANCE = new LoginService()
        }

        return LoginService.INSTANCE
    }

    getMe() {
        return axios
            .get(LoginService.API_LINK + '/me', {
                headers: {
                    'APP_TOKEN': this.token
                }
            })
    }

    getAllUser() {
        return axios.get<UserDto[]>(LoginService.API_LINK + '/allusers', {
            headers: {
                'APP_TOKEN': this.token
            }
        })
    }

    login() {
        if(this._isLogin) {
            return;
        }
        const email: string = document.querySelector<HTMLInputElement>("#email")!.value
        const password: string = document.querySelector<HTMLInputElement>("#password")!.value

        if(!email || !password) {
            return;
        }

        axios
            .post(LoginService.API_LINK + '/login', { email: email, password: password })
            .then((response) => {
                const login = LoginService.instance
                login.token = response.data.access_token

                login.isLogin = true

                const loginForm = document.querySelector<HTMLDivElement>("#login-form")!
                loginForm.classList.toggle('d-none')

                const chat = document.querySelector<HTMLDivElement>("#chat")!
                chat.classList.toggle('d-none')

                LoginService.instance._onLogin()
            })
            .catch(console.warn)
    }

    get token(): string {
        return this._token;
    }

    set token(value: string) {
        this._token = value;
    }

    get isLogin(): boolean {
        return this._isLogin;
    }

    set isLogin(value: boolean) {
        this._isLogin = value;
    }

    onLogin(callback: Function) {
        this._onLogin = callback
    }
}
