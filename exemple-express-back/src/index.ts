import express from 'express'
import * as dotenv from 'dotenv'
import {CWMChat} from "@chatwithme/server";
import cors from 'cors'
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import {users} from "./user.mock";
import jwt from 'jsonwebtoken'

dotenv.config()

const app = express()
// @ts-ignore
const port: number = 3000 || process.env.PORT
const chatBackModule: CWMChat = CWMChat.instance

chatBackModule.appToken = process.env.CHAT_TOKEN!

app.use(cors())
app.use(bodyParser.json())
app.use(cookieParser())
app.use(express.urlencoded({ extended: true }))

app.use((req, res, next) => {
    const token = req.header('APP_TOKEN')

    if(!token) {
        next()
    } else {
        jwt.verify(
            token,
            process.env.SECRET!,
            (err: any, decoded: any) => {
                if(!err) {
                    req.body.user = users.find(u => u.id === decoded.id)
                }
                next()
            }
        )
    }
})

app.get('/me', (req, res) => {
    if(!req.body.user) {
        res.status(400).send()
    } else {
        res.status(200).json(req.body.user)
    }
})

app.post('/login', (req, res) => {
    if(req.body.user != undefined) {
        return res.status(400).json({ message: 'Already connected' })
    }

    if (!req.body.email || !req.body.password) {
        return res.status(400).json({ message: 'Error. Please enter the correct username and password' })
    }

    const user = users.find(u => u.email === req.body.email && u.password === req.body.password)

    if(!user) {
        return res.status(400).json({ message: 'Error. Wrong login or password' })
    }

    const token = jwt.sign(
        {
            id: user.id,
            name: user.name
        },
        process.env.SECRET!)

    res.json({ access_token: token })
})

app.post('/chat', (req, res) => {

    if(req.body.user == undefined) {
        res.status(400).json({ message: "You are not logged."})
    }

    chatBackModule.generateCWMUserToken({
        userId: req.body.user.id + "",
        userName: req.body.user.name
    }).then(r => {
        res.json(r)
    }).catch(console.log)
})

app.get('/allusers', (req, res) => {
    if(req.body.user == undefined) {
        res.status(400).json({ message: "You are not logged."})
    }

    res.status(200).json(users.filter((user) => user.id != req.body.user.id))
})

app.listen(port, () => {
    console.log(`Express open, listening on port ${port}`)
})

