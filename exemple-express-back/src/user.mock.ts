export const users = [
    {
        id: '4b5210f2-d092-4ddd-9daf-c733937748eb',
        email: 'john.doe@gmail.com',
        name: 'John Doe',
        password: 'test1234'
    },
    {
        id: '89a1a2a4-cfa2-4fe4-a1f7-e12db88e9336',
        email: 'david.poe@gmail.com',
        name: 'David Poe',
        password: 'test1234'
    },
    {
        id: '62c96c0d-fa5c-45e5-838e-5aa93522d8f4',
        email: 'patrick.foe@gmail.com',
        name: 'Patrick Foe',
        password: 'test1234'
    },
    {
        id: 'b6230816-cac1-41be-8c94-f7e47e185623',
        email: 'michael.cow@gmail.com',
        name: 'Michael Cow',
        password: 'test1234'
    }
]
