# ChatWithMe example project
## Installation
Start by cloning the project on your environment. Then take the token you got when creating your application in your [ChatWithMe dashboard](https://chatwithme.fr/user-admin/dashboard) ([For more details](lien de la doc ta mère)). Put it in the .env file :
```env
CHAT_TOKEN=<your-cwm-token>
SECRET=5cf2bc88-3eb1-49f0-98a1-58f88c797a06
```
> The Secret here is used by the api to generate JWT token for the example application. You can change it if you want.

After setting up your token you can launch the example by using the following command
```
docker-compose up -d
```

## Usage
Go to your [localhost](http://localhost) and connect to one of the four example account :
- John Doe
  - email: john.doe@gmail.com
- David Poe 
  - email: david.poe@gmail.com
- Patrick Foe 
  - email: patrick.foe@gmail.com
- Michael Cow 
  - email: michael.cow@gmail.com

> The password is the same for all of them : test1234

Now you can "chat" with yourself. 
You can open multiple navigator tabs as each one is a session. That way you could have four tabs open, each one with a different account.